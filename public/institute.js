
function init_institute_interface(institute_data)
{
	function update_myself() {
      yearly_institute_BP.update(g_year,g_value_type);
      allY_institute_BP.update(g_value_type);
    }

	function assemble_institute_data(selected_year) {

      var res = {
        travels: clone(institute_data[selected_year].travels, 0.001) // kg -> T
        
      };

      return res;
    }

	var yearly_institute_BP   = make_yearly_barplot(null, '2017',g_value_type,'institute',(y,v) => compute_CO2_and_kWh(assemble_institute_data(y)),{dir:'horizontal', width: 800});
	var allY_institute_BP = make_all_years_barplot(institute_data, g_value_type,'institute',
	    {click: function(d) {
	        document.getElementById('input_institute_year').value = d.year;
	        yearly_institute_BP.update(d.year,g_value_type);
	    }});

  	{
  		var year_selector = document.getElementById('input_institute_year');
		for(var k in institute_data) {
			var opt = document.createElement('option');
			opt.value = k;
			opt.innerHTML = k;
			year_selector.appendChild(opt);
		}
		year_selector.value = '2017';
		year_selector.addEventListener('change',function(){ yearly_institute_BP.update(this.value,g_value_type); });
	}

	update_myself();
}