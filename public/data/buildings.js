
var buildings = {

  inria_bso_main: {
    name: "Inria Bordeaux - principal",

    elec: 1540000, // moyenne sur les dernières années

    // nombre de place théorique (ex. 2 par bureau, 12 par open-space, etc.)
    seats: 9*(7*2+12) + (3*2+2*6+3) /*R5*/ + (6+6+3) /*R1*/ + (2+4) /*R2*/ + (2*7+3+6) /*R4*/, // = 299

    // nombre d'agent réellement dans le batiment
    workers: 200,  // au pif
  },

  
};