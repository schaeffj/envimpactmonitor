
var stats = {

	// CO2 footprint of an average French citizen
	default_french_citizen: {
      // sources:
      //    http://ravijen.fr/?p=440
      //    https://e-rse.net/emissions-CO2-francais-empreinte-carbone-271641/#gs.tzd03e
      //    https://www.gouvernement.fr/indicateur-emprunte-carbone
      //    https://www.data.gouv.fr/fr/datasets/les-nouveaux-indicateurs-de-richesse-1/
      //    https://www.ipsos.com/fr-fr/lobservatoire-du-bilan-carbone-des-menages
      // NOTE -> il me semble que ces études ingorent les émmissions de CO2 du bois-énergie (+787 !!!)
      french_transport_others_CO2: 100,
      french_transport_flight_CO2: 500,
      french_transport_car_CO2: 2000,
      french_meals_CO2: 2350,
      french_home_energy_CO2: 1700,
      french_things_devices_CO2: 1000,
      french_things_clothes_CO2: 770,
      french_things_others_CO2: 720,
      french_home_build_CO2: 630,
      french_home_equipment_CO2: 340,
      
      french_transport_distribution_CO2: 390,
      french_services_CO2: 1500
    },

    // distribution of average distances from home to work in France
    office_way_average_dist: { // source: https://fr.statista.com/statistiques/995652/distance-moyenne-parcourue-francais-trajet-quotidien/
        lt_5:     24, // was 21/93
        in_5_14:  36, // was 31/93
        in_15_29: 22, // was 20/93
        in_30_49: 10, // was 11/93
        in_50_100: 7, // was 8/93
        gt_100:    1, // was 2/93
	},

	// distribution of transportation kind per distance
	office_way_per_dist: {  // source: insee.fr & http://ravijen.fr/?p=440
	                      	// need to be checked and fine tuned
		lt_5:       {car: 64, motorbike: 3, bus: 14, train:  3, walk: 10, bike: 6},
		in_5_14:    {car: 78, motorbike: 3, bus: 10, train:  6, walk:  0, bike: 3},
		in_15_29:   {car: 82, motorbike: 4, bus:  7, train:  7, walk:  0, bike: 0},
		in_30_49:   {car: 82, motorbike: 3, bus:  2, train: 13, walk:  0, bike: 0},
		in_50_100:  {car: 80, motorbike: 2, bus:  0, train: 18, walk:  0, bike: 0},
		gt_100:     {car: 70, motorbike: 1, bus:  0, train: 29, walk:  0, bike: 0},
	},

	// mean distance per distance range
	office_way_dist: {
		lt_5: (0+5)/2.,
		in_5_14: (5*4+14*2)/6.,
		in_15_29: (15*5+29*2)/7.,
		in_30_49: (30*4+49*2)/6.,
		in_50_100: (50*4+99*2)/6.,
		gt_100: 100,
	},

  bdx_to_office: {
    // version basée sur le sondage mobilité 2018 + un peu de pif pour extrapoler
    avg_dist: {
      car:    12.5,
      ter:    55,
      tram:    4.7,
      bus:     5.3,
      ebike:   7,
      bike:    5,
      walk:    1.5
    },
    ratio: {
      car:    0.49,
      ter:    0.025,
      tram:   0.155,
      bus:    0.055,
      ebike:  0.085,
      bike:   0.1,
      walk:   0.09
    }
  },

};