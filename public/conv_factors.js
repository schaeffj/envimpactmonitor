

// type= CO2 or kWh
// this function is essentially a wrapper around conv.to_"type"[name]
// but with an analysis of name to catch already converted factors.
// For instance:
//  - if type==CO2 and name includes CO2, then there is noting to convert.
//  - if name includes "elec", then we replace name by "elec"
//  - if name is a meal, then we prefix it by "meal_"
function convfactor(name,type) {
  if(name.includes('elec'))
    name = 'elec'
  else if(['beef','chicken','fish','vege','vegan'].includes(name))
    return conv['to_'+type]['meal_'+name]; // FIXME, we shoudld directly use meal_XXXX
  else if(name.includes('CO2'))
    if(type=='CO2')
      return 1;
    else {
      // FIXME: is returning 0 correct here?
      // if(!( name.includes('french') || name.includes('grey') ))
        // console.log("WARNING: returning 0 while converting " + name + " to " + type);
      return 0;
    }
  else if(name.includes('kWh') && type=='kWh')
    return 1;
  
  return conv['to_'+type][name];
}

/* ressources

- [1] https://ecf.com/news-and-events/news/how-much-co2-does-cycling-really-save (cycling in Europe)
- [2] https://ecf.com/files/wp-content/uploads/ECF_BROCHURE_EN_planche.pdf
  - bus ride = 101g/km
  - car production: 42g/km (5-7 tons CO2 per vehicle, maintenance excluded, ADEME)
  - car ride: 271g/km (include production, ride only range from 160 to 260, weighted average of 229)
  - bicycle production: 5g/km
  - bicycle ride (=food): 16g/km (much less for vegetarian!, but food should not be considered IMO)
  - pedelec by TNO (2400km/y, life cycle of 8y)
    - production: 7g/km
    - elec: 10g/km (should be much less in France!, 8g at most)
    - total: 15g/km

- [3] Base carbone & http://avenirclimatique.org/micmac/sources.php
  - 17.6 kgCO2e/kg pour la vache de réforme
  - 25 kgCO2e/kg pour veau, bœuf, mouton
  - 5 kgCO2e/kg pour porc
  - 2 kgCO2e/kg poisson
  - 10 kgCO2e/kg fromage/beurre
  - 2.4 kgCO2e/kg produits laitiers
  - 1.2 kgCO2e/kg lait
  - 2.8 kgCO2e/kg légume/fruit hors saison/non local/exotique
  - 0.11 kgCO2e/kg légume/fruit locaux de saisons
  - 2.7 kgCO2e/kg plat cuisiné/conserve [source casino]
  - 

*/


var wood_PCI = 4;           // kWh/kg includes efficiency ratio
var wood_stere_in_kg = 550; // weight of 1 stere

var conv = {

  // convert anything to kg of CO2 equivalent
  to_CO2:{

    flight:     0.124,  // approx 0.32+011*x for x>700, no contrail
    flight_loc: 0.172,   // approx 0.32+011*x for x<700, no contrail
    car:        0.26,   // divide by number of passengers
    ecar:       0.1,    // divide by number of passengers - labos1p5
    train:      0.01,   // ADEME (http://www.bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=train)
                        //  TGV :       0.0037
                        //  Train :     0.0056
                        //  TER :       0.009 (élec) - 0.08 (diesel)
                        //  Allemagne : 0.067
                        // Les valeurs pour la France me semblent bien trop optimistes, on a plus qu'un facteur 10 avec l'Allemagne
                        // alors que les émissions de l'électricité sont plutot d'un facteur 6 et qu'il faut prendre en compte
                        // les infrastructures et l'énergie grise. D'où le choix empirique et arbitraire de 0.01
                        // Voir aussi
                        // - https://fr.wikipedia.org/wiki/%C3%89missions_de_CO2_des_transports_ferroviaires_en_France
                        // - https://www.economie.gouv.fr/files/files/directions_services/cge/verdissement_flotte_ferroviaire.pdf

    tgv:        0.007,  // + 1 fab + 2 infra
    ter:        0.04,   // 56% elec, 44% diesel

    bus:        0.068,  // https://www.eea.europa.eu/media/infographics/co2-emissions-from-passenger-transport/view, ADEME 0.16

    tram:       0.0076, // labo1p5

    motorbike:  0.15,   // TODO (très très variable, par ex. 2-3l au 100 pour les petits scooter les + économes, à 6l au 100 pour les gros scooter & motos)
    ebike:      0.015,  // includes production + maintenance + electricity [1,2]
                        // - si 1 charge = 60km = 0.4kWh et rendement de 70% alors 0.0095 kWh/km => 0.0008 kgCO2e/km
                        // - reste l'énergie grise, si on prend:
                        //    - 400kgCO2e/vélo (arbitraire, 240 kg pour un vélo ici : https://slate.com/technology/2011/08/how-soon-does-a-bike-pay-back-its-initial-carbon-footprint.html)
                        //    - 1000 cycles pour une batterie Li (soit 60000km)
                        //    - 60000 km pour le vélo complet
                        //    - 10kgCO2e tout les 3000 km pour l'entretien (arbitraire) soit 200kWh en plus
                        //   -> 0.01 kgCO2e/km
    bike:       0.005,  // includes production & maintenance only [1,2]
    walk:       0, 

    desktop:        devices.desktop.grey_CO2,
    laptop:         devices.laptop.grey_CO2,
    screen:         devices.screen.grey_CO2,
    pad:            devices.pad.grey_CO2,
    smartphone:     devices.smartphone.grey_CO2,
    ipphone:        devices.ipphone.grey_CO2,
    printer:        devices.printer.grey_CO2,
    videoprojector: devices.videoprojector.grey_CO2,

    TV:         400,    // [340:500], http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=t%C3%A9l%C3%A9vision

    watch:       40,    // Apple Watch s5 : 40 including 13% usage - https://www.apple.com/environment/

    console:        75,   // http://bilans-ges.ademe.fr
                          // portable: 30

    ereader:        44,   // http://bilans-ges.ademe.fr

    box:            80,   // http://bilans-ges.ademe.fr: ADSL haut-débit 60 + box TV...
    decoder:        60,   // http://bilans-ges.ademe.fr
    modem_fiber:    90,   // http://bilans-ges.ademe.fr
    homecinema :   130,   // http://bilans-ges.ademe.fr

    photocopier:  3000,    // +/- 50% https://ecoinfo.cnrs.fr/wp-content/uploads/2019/12/ecodiag-v19.12.html

    
    gpu:            100,  // TODO au pif, ici on considère uniquement les GPUs haut de gamme, sinon ils sont inclus dans les PC 

    dishwasher:     270,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Lave-vaisselle
    washer:         340,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Lave-linge
    dryer:          300,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=S%C3%A8che-linge
                          // - evacuation: 265
                          // - condensation: 300
                          // - PAC: 300 (???)
    oven:           220,  // encastrable, http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=four
    microwave:      100,  // +/- 50% http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Micro-onde
    fridge:         260,  // combiné,  http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=R%C3%A9frig%C3%A9rateur
    freezer:        350,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Cong%C3%A9lateur
                          // - armoire 415 +/- 30%
                          // - coffre 300 +/- 30%
    vacuum:          50,  // +/- 30% http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Aspirateur+m%C3%A9nager
    mixer:           41,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Robot+multifonction
    breadmaker:      50,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=Machine+%C3%A0+pain
    yogurtmaker:     22,  // http://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element?recherche=yaourti%C3%A8re

    COP21:      1,

    elec:       0.084,  // kWh, valid in France only! ADEME mix-moyen 2018: 0.057 ; IEA 2018: 0.035
                        //  - ADEME chauffage : 0.17
                        //  - ADEM clim ter.  : 0.043
                        // débat: http://www.economiedenergie.fr/le-contenu-en-co2-du-kwh-%C3%A9lectrique.html
                        //        entre 40 et 180 selon la saison, entre 400 et 700 pour une autre méthode de calcul...
                        //        voir aussi http://www.carbone4.com/analyse-chaudieres-gaz-climat/
                        // https://reseaudurable.com/bilan-carbone-production-electricite-france/
                        //  - charbon: 1
                        //  - fioul: 0.73
                        //  - gaz:   0.418
                        //  - éolien: 0.013 (+/- 50%)
                        //  - photovoltaique: 0.055
                        //  - hydro: 0.006
                        //  - nucléaire: 0.006
                        // Pleins de sources ici: https://www.contrepoints.org/2014/02/16/156807-eoliennes-quel-est-leur-vrai-bilan-carbone
                        //  - charbon: 1 (entre 0.8 et 1.2)
                        //  - gaz= entre 0.450-0.6
                        //  - éolien: entre 0.060 et 0.1
                        //  - nucléaire: entre 0.01 et 0.1, moyenne à 0.022
                        //  - hydro: 0.006 ou 0.035 si on prend en compte les émissions des terres immergées 
                        // Mix en France 2015 : [hydro:11 nucl:76.3 fossil:6.2 éolien:4 photo:1.4]
                        // Mix en France 2018 : [hydro:12.5 nucl:71.7 gaz:5.7 éolien:5.1 photo:1.9 bio:1.8 charbon:1.1 fioul:0.4]
                        //  - https://www.connaissancedesenergies.org/bilan-electrique-de-la-france-que-retenir-de-2018-190214
                        //  - 17 journées importatrices, contre 52 en 2017, soit 66 TWh (5.4%)
                        //  - facteur de charge solaire 14%
                        //  - si on suppose que l'on exporte plutot le nucl, ça donne en moyenne: 0.084 khCO2e/kWh
                        //     = [12.5 (71.7-5.4) 5.7 5.1 1.9 1.8 1.1 0.4 5.4] * [0.006 0.006 0.42 0.013 0.055 0.4 1 0.73 0.6]' / 100

    elec_eu:    0.51,   // kWh, in Europe, https://ferme.yeswiki.net/wikis/Empreinte, IEA 2018: 0.276
    gaz_nat_m3: 2.08,   // input=m^3, gaz nat, https://www.picbleu.fr/page/comparatif-pouvoir-calorifique-gaz-fioul-bois-charbon
    gaz_pp_kg:  3.17,   // input=kg,  gaz propane, "
    gaz_nat_kWh:0.2,    // "
    gaz_pp_kWh: 0.23,   // "
    wood_kg:    1.83,   // Sources:
                        // - http://documents.irevues.inist.fr/bitstream/handle/2042/47204/LETURCQ.pdf?sequence=1
                        //    (Concawe/Eucar/JRC, 2007 ; IPCC, 2006)
                        //    -> 1.83 kgCO2e / kg
                        // - https://www.ademe.fr/sites/default/files/assets/documents/34702_acv_bois_dom.pdf
                        //    -> ~0.04 kgCO2e / kWh utile -> 0.18 / kg
                        //    -> mais les émmissions de CO2 semblent ignorées (replantage)
                        // - http://www.economiedenergie.fr/les-%C3%A9missions-de-co2-par-%C3%A9nergie.html
                        //   -> 0.013 kgCOEe / kWh si inclut captation, 0.335 / kWh -> 0.335 / 0.7 (rendement) * 4.5 (PCI) -> 2.15 !!

    heatnet:    0.1,    // reseau de chaleur, moyenne des données labos1p5/base-carbone. Valeurs entre 0.02 et 0.38.

    // source: http://document.environnement.brussels/opac_css/elecfile/STUD2013Gobelet.PDF
    goblet_pp:    0.025, // per unit
    goblet_pla:   0.011,
    ecocup:       0.13,
    goblet_glass: 0.4,

    bottle_pet:   0.175,  // bouteille de 1.5l
    bottle_glass: 0.1,    // bouteille de 1l

    bottle_pet25: 0.08,   // bouteille de 0.25l
    can_33:       0.066,  // cannette de 0.33l

    // http://www.bilans-ges.ademe.fr/documentation/UPLOAD_DOC_FR/index.htm?repas.htm
    vegetable:     1.3,   // for 1 kg
    beef_kg:      27,     // kg meat to kg CO2e
    fish_kg:       5,       // kg fish to kg CO2e

    // kg CO2 for 100 kilo Calories
    kcal_beef:    1.43,   // Pimentel [2008]
    kcal_lamp:    0.97,   // Pimentel [2008]
    kcal_salmon:  0.48,   // Pimentel [2008]
    kcal_eggs:    0.39,   // Pimentel [2008]
    kcal_pork:    0.30,   // Pimentel [2008]
    kcal_milk:    0.24,   // Pimentel [2008]
    kcal_chicken: 0.52,   // Pimentel [2008]
    kcal_tomatoes:0.046,  // Pimentel [2008]
    kcal_potatoes:0.023,  // Pimentel [2008]
    kcal_corn:    0.011,  // Pimentel [2008]

    // http://www.bilans-ges.ademe.fr/documentation/UPLOAD_DOC_FR/index.htm?repas.htm
    meal_beef:      5.0,  // beef @ 25 gCO2/g rather than 35 gCO2/g
    meal_chicken:   1.35,
    meal_fish:      1.35,   // poisson ~ poulet
    meal_vege:      0.51,
    meal_vegan:     0.3,  // au pif

    hotel_night:  7,      // https://www.consoglobe.com/impact-ecologique-d-une-nuit-d-hotel-cg
                          
  },

  to_kWh:{     // convert km to energy consumption equivalent
    flight:     0.52,
    flight_loc: 0.52,   // should be more....
    car:        0.63,   // divide by number of passengers
    train:      0.04,
    tgv:        0.04, // TODO
    ter:        0.04, // TODO
    bus:        0,    // TODO
    ebike:      0,    // TODO
    bike:       0,    // TODO

    desktop:    1000, // pif
    laptop:     1000, // pif
    screen:     900,  // pif

    COP21:      0,    // TODO
    elec:       1,    // FIXME finale vs primaire ?
    elec_eu:    1,    // TODO
    gaz_nat_m3: 10.4, // input=m^3
    gaz_pp_kg:  13.8, // input=kg
    wood_kg:    wood_PCI,

    toe:        11630,
  },

  to_kg: {
    gaz_pp_kg:    1,
    gaz_pp_kWh:   1./13.8,
    wood_kg:      1,
    wood_stere:   wood_stere_in_kg,
    wood_kWh:     1./wood_PCI,
  },
  to_m3: {
    gaz_nat_m3:   1,
    gaz_nat_kWh:  1./10.4,
  },
  to_stere: {
    wood_stere: 1,
    wood_kg:    1./wood_stere_in_kg,
  },

  // kWh_to_CO2:{    // convert electricity consumption to kg of CO2 equivalent
  //   elec:     0.11,   // valid in France only!
  //   elec_eu:  0.51    // in Europe, https://ferme.yeswiki.net/wikis/Empreinte
  // },

};