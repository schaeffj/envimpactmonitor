

// TODO: use a public API: http://transport.opendata.ch/
//      or: https://docs.distance.to/

// see: https://www.icao.int/en vironmental-protection/CarbonOffset/Pages/default.aspx

var distances={
   "Francfort": {
      "flight": 910,
      "train": 1300,
      "car": null,
      "local": true
   },
   "Grenoble": {
      "flight": null,
      "train": 1050,
      "car": null,
      "local": true
   },
   "Lyon": {
      "flight": 550,
      "train": 1000,
      "car": null,
      "local": true
   },
   "Nancy": {
      "flight": null,
      "train": 850,
      "car": null,
      "local": true
   },
   "Paris": {
      "flight": 500,
      "train": 550,
      "car": null,
      "local": true
   },
   "Poitiers": {
      "flight": null,
      "train": 220,
      "car": null,
      "local": true
   },
   "Rennes": {
      "flight": 380,
      "train": 860,
      "car": null,
      "local": true
   },
   "Toulouse": {
      "flight": null,
      "train": 250,
      "car": null,
      "local": true
   },
   "Amsterdam": {
      "flight": 1000,
      "train": null,
      "car": null,
      "local": true
   },
   "Chengdu": {
      "flight": 9000,
      "train": null,
      "car": null,
      "local": false
   },
   "Helsinki": {
      "flight": 2400,
      "train": null,
      "car": null,
      "local": false
   },
   "Los Angeles": {
      "flight": 10000,
      "train": null,
      "car": null,
      "local": false
   },
   "Milan": {
      "flight": 800,
      "train": null,
      "car": null,
      "local": true
   },
   "Moliets": {
      "flight": null,
      "train": null,
      "car": 200,
      "local": true
   },
   "Nice": {
      "flight": 600,
      "train": null,
      "car": null,
      "local": true
   },
   "Seignosse": {
      "flight": null,
      "train": null,
      "car": 200,
      "local": true
   },
   "Tokyo": {
      "flight": 10000,
      "train": null,
      "car": null,
      "local": false
   }
};

